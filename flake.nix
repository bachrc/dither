{
  description = "Lyabox";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.rust-overlay.url = "github:oxalica/rust-overlay";
  inputs.rust-overlay.inputs.flake-utils.follows = "flake-utils";
  inputs.rust-overlay.inputs.nixpkgs.follows = "nixpkgs";

  inputs.crane.url = "github:ipetkov/crane";
  inputs.crane.inputs.flake-utils.follows = "flake-utils";
  inputs.crane.inputs.nixpkgs.follows = "nixpkgs";
  inputs.crane.inputs.rust-overlay.follows = "rust-overlay";

  outputs =
    { self
    , nixpkgs
    , flake-utils
    , rust-overlay
    , crane
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          rust-overlay.overlays.default
        ];
      };

      pythonPackages = pkgs.python310Packages;
      pythonImpureTools = [
        pythonPackages.venvShellHook
      ];


      rust-toolchain = (pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml);

      devShell = pkgs.mkShell {
        packages = [
          rust-toolchain
          pkgs.mask
          pkgs.openssl
        ];
      };

      craneLib = (crane.mkLib pkgs).overrideToolchain rust-toolchain;

      src = craneLib.cleanCargoSource ./.;

      rust-dependencies = craneLib.buildDepsOnly {
        inherit src;
      };

      rust-package-binary = craneLib.buildPackage {
        inherit src;
        cargoArtifacts = rust-dependencies;

        doCheck = false;
      };

      app = flake-utils.lib.mkApp {
        drv = rust-package-binary;
      };
    in
    {
      # run with `nix develop`
      devShells.default = devShell;

      # run with `nix shell`
      packages.default = rust-package-binary;

      # run with `nix run`
      apps.default = app;
    });
}
